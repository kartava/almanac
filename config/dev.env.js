'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')


module.exports = merge(prodEnv, {
  NODE_ENV: JSON.stringify('development'),
  BASE_URL: JSON.stringify('http://localhost:8000/api/')
})
