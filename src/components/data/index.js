const albums = [
  {
    'id': 2,
    'artist': {
      'id': 2,
      'name': 'David Bowie',
      'link': 'http://google.com'
    },
    'tracks': [
      {
        'id': 1,
        'name': 'Blackstar',
        'track': null,
        'length': '00:09:57',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 2,
        'name': 'Tis a Pity She Was a Whore',
        'track': null,
        'length': '00:04:52',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 3,
        'name': 'Lazarus',
        'track': null,
        'length': '00:06:22',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 4,
        'name': 'Sue (Or in a Season of Crime)',
        'track': null,
        'length': '00:04:40',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 5,
        'name': 'Girl Loves Me',
        'track': null,
        'length': '00:04:51',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 6,
        'name': 'Dollar Days',
        'track': null,
        'length': '00:04:44',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      },
      {
        'id': 7,
        'name': 'I Canâ€™t Give Everything Away',
        'track': null,
        'length': '00:05:47',
        'released_date': '2018-07-15',
        'is_single': false,
        'album': 2
      }
    ],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'Blackstar',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/4ee16a2b83df4b6daca65a7a21a3c687.png',
    'length': '00:42:12',
    'recording_start_date': '2018-07-14',
    'recording_end_date': '2018-07-14',
    'released_date': '2018-07-14'
  },
  {
    'id': 1,
    'artist': {
      'id': 1,
      'name': 'Red Hot Chili Peppers',
      'link': 'http://google.com'
    },
    'tracks': [],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'The Gateway',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/1eeed0074a734182bd8fb250592b8354.jpg',
    'length': '00:53:48',
    'recording_start_date': '2018-07-14',
    'recording_end_date': '2018-07-14',
    'released_date': '2016-04-17'
  },
  {
    'id': 4,
    'artist': {
      'id': 2,
      'name': 'David Bowie',
      'link': 'http://google.com'
    },
    'tracks': [],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'The Next Day(Deluxe)',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/8f099840764e4c4eb85bf033b2696665.jpg',
    'length': '00:01:00',
    'recording_start_date': '2018-07-15',
    'recording_end_date': '2018-07-15',
    'released_date': '2018-07-15'
  },
  {
    'id': 5,
    'artist': {
      'id': 1,
      'name': 'Red Hot Chili Peppers',
      'link': 'http://google.com'
    },
    'tracks': [],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'Stadium Arcadium',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/da155362f1e4469ca1063a4fe23ea2e0.jpg',
    'length': '00:01:00',
    'recording_start_date': '2018-07-15',
    'recording_end_date': '2018-07-15',
    'released_date': '2018-07-15'
  },
  {
    'id': 6,
    'artist': {
      'id': 1,
      'name': 'Red Hot Chili Peppers',
      'link': 'http://google.com'
    },
    'tracks': [],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'I am With You',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/f2caa453fd334808bfc4e0cc61dca7ab.jpg',
    'length': '00:00:54',
    'recording_start_date': '2018-07-15',
    'recording_end_date': '2018-07-15',
    'released_date': '2018-07-15'
  },
  {
    'id': 3,
    'artist': {
      'id': 2,
      'name': 'David Bowie',
      'link': 'http://google.com'
    },
    'tracks': [],
    'producers': [
      {
        'id': 1,
        'name': 'Rick R.',
        'link': 'http://google.com'
      }
    ],
    'genre': {
      'id': 1,
      'name': 'Rock',
      'link': 'http://google.com'
    },
    'studio': {
      'id': 1,
      'name': 'Cool Studio',
      'link': 'http://google.com'
    },
    'label': {
      'id': 1,
      'name': 'Cool Label',
      'link': 'http://google.com'
    },
    'name': 'The Next Day',
    'cover': 'http://localhost:8000/media/albums/2018/07/15/63ce7a73b9ee42d79d712dbf855367e1.jpg',
    'length': '00:55:10',
    'recording_start_date': '2018-07-15',
    'recording_end_date': '2018-07-15',
    'released_date': '2018-07-15'
  }
]

export default albums
