import axios from 'axios'

class Api {
  doRequest(method, url, data) {
    const headers = {
      'Content-Type': 'application/json'
    }
    const token = localStorage.getItem('token')
    if (token) { headers.Authorization = 'Token ' + token }
    return axios({
      method: method,
      url: process.env.BASE_URL + url,
      data: data,
      headers
    })
  }

  get(url, data) {
    return this.doRequest('GET', url, data)
  }
  post(url, data) {
    return this.doRequest('POST', url, data)
  }
  del(url, data) {
    return this.doRequest('DELETE', url, data)
  }
  put(url, data) {
    return this.doRequest('PUT', url, data)
  }
  patch(url, data) {
    return this.doRequest('PATCH', url, data)
  }
  options(url, data) {
    return this.doRequest('OPTIONS', url, data)
  }
}

export default new Api()
