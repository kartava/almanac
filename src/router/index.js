import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/components/Login'
import Registration from '@/components/Registration'
import AlbumList from '@/components/AlbumList'
import AlbumDetail from '@/components/AlbumDetail'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/registration',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/albums',
      name: 'Albums',
      component: AlbumList
    },
    {
      path: '/albums/:id',
      name: 'AlbumDetail',
      component: AlbumDetail
    }
  ]
})
